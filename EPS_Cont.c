 //THE MATRIX CALCULATOR (an idea taken from https://matrixcalc.org/)
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/**
    The program will be able perform basic algebric operations with matrixes,
    get the determinant of the matrix, find the sum and multiply the matrixes,
    calculate the inverse matrix and transposition.

**/
void selection (void);
void input_rand(float ** array, int n, int m);
void input_keys(float ** array, int n, int m);
void output(float **array, int n, int m);
void matrix_multiplication(float ** arr1, float ** arr2, float ** res, int n, int m);
void matrix_addition(float ** arr1, float ** arr2, float ** res, int n, int m);
void subtraction(float ** arr1, float ** arr2, float ** res, int n, int m);
void number_deduction(float ** array, float ** res, int n1, int m1, int n2, int m2);
void number_multiplication(float ** array,float ** res, int n, int m);
void number_addition(float ** array, float ** res, int n, int m);
void inverse_matrix(float ** array, float ** res, int n, int m);
void transposition(float ** array, float ** res, int n, int m);
float determinant(float ** a, int n);
float minor (float ** array, int n, int lin, int col);
float ** memory(float ** array, int n, int m);
float correctFill(char number);
int correct(char number);
int correctSel(char number);
/**
    The basic function:

    1) selection                 the procedure of selection of the type of the matrixes and the way for filling them
    2) determinant               the function of detection of the determinant of the matrix
    3) inverse matrix            the function of determining the inverse matrix
    5) matrix multiplication     the procedure of multiplication by  matrixes with the output on the display.
    6) number multiplication     the procedure of multiplication of matrix by a number
    7) matrix addition           the matrix addition procedure
    8) number addition           the procedure of adding of  the matrix to the number
   10) transposition             the procedure of the matrix transposition
   11) memory                    the procedure of allocation for the dynamic memory
   13) main                      the main function
**/
int main()
{
    int flag = 0;
      do{
            selection();
            printf("\n Press: 'any key' to start from the beginning \n\n or '0' to complete the program:  ");
            scanf("%d",&flag);
        }while(flag);
            printf("\n\n THE END!");

   getch();
 return 0;
}

void selection (void)
{
/**
	This is a basic unit of selectivity that allows the user to select the type, the size and
    the method	of matrix filling. A method of filling may be selected
    as manual - ( with the help of the keyboard), or by generating random numbers, when it is  necessary
    to show the algebric transformation of the matrixes.
    There is  the choice of the operation / multiplication, addition will be offered here,etc. / with
    subsequent transfer to the appropriate function depending on the selection.
**/
    int i = 0;
    int NA, MA = 0;
    int NB, MB = 0;
    int NR, MR = 0;
    int select = 0;
    int operation = 0;
    float ** matrix1 = NULL;
    float ** matrix2 = NULL;
    float ** result  = NULL;
    char time = 0;

                 printf("\n");
          do{
                 printf(" Enter the number of rows matrix A :");
                 scanf("%c", &time);
                 fflush(stdin);
                 NA = correct(time);
            }while(NA == 'a');

          do{
                 time = 0;
                 printf(" Enter the number of coll matrix A :");
                 scanf("%c", &time);
                 fflush(stdin);
                 MA = correct(time);
            }while(MA == 'a');

            matrix1 = memory(matrix1, NA, MA);
            printf("\n");

           do{
                 printf(" Enter the number of rows matrix B :");
                 scanf("%c", &time);
                 fflush(stdin);
                 NB = correct(time);
             }while(NB == 'a');
           do{
                 printf(" Enter the number of coll matrix B :");
                 scanf("%c", &time);
                 fflush(stdin);
                 MB = correct(time);
             }while(MB == 'a');

            matrix2 = memory(matrix2,NB, MB);

              if(NA > NB)NR = NA;else NR = NB;
              if(MA > MB)MR = MA;else MR = MB;
            result = memory(result, NR, MR);

             printf ("\n Select the method of filling:\n\n");
             printf (" Keys: '1'\n Rand: '2'\n ");
        do{
             time = 0;
             printf(" Input select matrix A:  ");
             scanf("%c", &time);
             fflush(stdin);
             select = correctSel(time);
          }while(select == 'a');

             printf("\n");
             if(select == 1){input_keys(matrix1, NA, MA); output(matrix1, NA, MA);}
               if(select == 2){input_rand(matrix1, NA, MA); output(matrix1, NA, MA);}

          do{
             time = 0;
             select = 0;
             printf("\n Input select matrix B:  ");
             scanf("%c", &time);
             fflush(stdin);
             select = correctSel(time);
            }while(select =='a');

             printf("\n");
             if(select == 1){input_keys(matrix2, NB, MB);output(matrix2, NB, MB);}
              if(select == 2){input_rand(matrix2, NB, MB);output(matrix2, NB, MB);}

            printf(" Select algebraic operation:\n\n"  );
            printf(" '1' - A * B \n");
            printf(" '2' - A + B \n");
            printf(" '3' - A - B \n");
            printf(" '4' - A * number \n");
            printf(" '5' - A + number \n");
            printf(" '6' - A - number \n");
            printf(" '7' - determinant\n");
            printf(" '8' - inverse mat\n");
            printf(" '9' - transposition \n");
     int flag = 0;
     do{
             printf("\n Select algebraic operation: ");
             scanf("%d", &operation);


    switch(operation)
    { int number = 0;

    case 1: if(MA == NB)
	        {matrix_multiplication(matrix1, matrix2, result, NB, MB); break;}
            else{printf("\n ERROR!!!The 'coll A' & 'rows B' must be same! \n");
			break;}
    case 2: if(NA == NB && MA == MB){matrix_addition(matrix1, matrix2, result, NA, MA);
	        break;}
            else {printf("\n ERROR!!!The matrix must have the same size! \n");
			break;}
    case 3: if(NA == NB && MA == MB){ subtraction   (matrix1, matrix2, result, NA, MA);
	        break;}
            else {printf("\n ERROR!!!The matrix must have the same size! \n");
			break;}
    case 4:  printf("\n matrix A press: '1' \n matrix B press: '2' \n RESULT   press: '3'");
	        scanf("%d", &number);
            if(number == 1){number_multiplication(matrix1,result, NA, MA);
			break;}
            if(number == 2){number_multiplication(matrix2,result, NB, MB);
			break;}
            if(number == 3){number_multiplication(result, result, NB, MB);
			break;}
    case 5: printf("\n matrix A press: '1' \n matrix B press: '2' \n RESULT   press: '3'");
	        scanf("%d", &number);
            if(number == 1){ number_addition(matrix1,result, NA, MA);
			break;}
            if(number == 2){ number_addition(matrix2,result, NB, MB);
			break;}
            if(number == 3){ number_addition(result, result, NB, MB);
			break;}
    case 6: printf("\n matrix A press: '1' \n matrix B press: '2' \n RESULT   press: '3'");
	        scanf("%d", &number);
            if(number == 1){number_deduction(matrix1, result, NA, MA, NB, MB);
			break;}
            if(number == 2){number_deduction(matrix2, result, NB, MB, NB, MB);
			break;}
            if(number == 3){number_deduction(result, result, NB, MB, NB, MB);
			break;}
    case 7: printf("\n matrix A press: '1' \n matrix B press: '2' \n RESULT   press: '3'");
	        scanf("%d", &number);
            if(number == 1 && NA == MA){printf("\n Det: %.2f \n\n", determinant (matrix1, NA));
			break;}
            if(NA != MA) {printf("\n ERROR! Matrix is not square! \n\n");
			break;}
            if(number == 2 && NB == MB){printf("\n Det: %.2f \n\n", determinant (matrix2, NB));
			break;}
            if(NB != MB) {printf("\n ERROR! Matrix is not square! \n\n");
			break;}
    case 8: printf("\n matrix A press: '1' \n matrix B press: '2' \n RESULT   press: '3'");
	        scanf("%d", &number);
            if(number == 1){inverse_matrix (matrix1, result, NA, MA);
			break;}
            if(number == 2){inverse_matrix (matrix2, result, NB, MB);
			break;}
            if(number == 3){inverse_matrix (result, result,  NB, MB);
			break;}
    case 9: printf("\n matrix A press: '1' \n matrix B press: '2' ");
	        scanf("%d", &number);
            if(number == 1){transposition  (matrix1, result, NA, MA);
			break;}
            if(number == 2){transposition  (matrix2, result, NB, MB);
			break;}
    }
    printf(" Press: 'any key' to operation to the 'RESULT' or '0' to extension: ");
    scanf("%d",&flag);
    }while(flag);

        for (i = 0; i < MB; i++){
        free (*(result + i));
       }
        free(result);
        for (i = 0; i < MB; i++){
        free (*(matrix2 + i));
       }
        free(matrix2);
	    for (i = 0; i < MA; i++){
        free (*(matrix1 + i));
       }
        free(matrix1);

}

float ** memory (float ** array, int n, int m)
{
/**
    The function inputs the  sizes of the matrix and provides  the allocation of the dynamic
    memory that later will be cleared in the right code area.
**/
    int i = 0;
            array = (float**)calloc(n,sizeof(float*));
            for (i = 0; i < n; i++)
            *(array + i) = (float*)calloc(m,sizeof(float));

         return array;
}


void input_rand (float ** array, int n, int m)
{
    int i = 0;
    int j = 0;
              printf("\n");
              for(i = 0;i < n; i++){
                for(j =0; j < m;j++){
                   *(*(array + i) + j) = rand() % 10;
                }
              }
}

void input_keys (float ** array, int n, int m)
{
    int i;
    int j;
    char timeFill = 0;
            for(i = 0; i < n; i++){
                for(j = 0; j < m; j++){
                do{
                    printf(" [%d][%d] ", i, j);
                    scanf("%c", &timeFill);
                    fflush(stdin);
                    array[i][j] = correctFill(timeFill);
                  }while(array[i][j] == 'a');
                }
            }
}

void output (float **array, int n, int m)
{
    int i = 0;
    int j = 0;
            printf("\n");
            for(i = 0; i < n; i++){
                for(j =0; j < m;j++){
                printf(" %7.2f",*(*(array + i) + j));
                }
                printf("\n\n");
            }
}

void matrix_multiplication (float ** arr1, float ** arr2, float ** res, int n, int m)
{
/**
    The function inputs the  pointers to teh arrays, and the sizes of the matrixes,
	multiply the matrix by the matrix, records the result to the additional array,
	and outputs the resulting matrix on the display.
**/

  int i = 0;
  int j = 0;
  int k = 0;
        printf("\n RESULT: \n");
        for(i = 0; i < n; i++)
        {
		  for(j = 0; j < m; j++)
          {
			res[i][j] = 0;
			for(k = 0; k < n; k++)
            {
				res[i][j] += arr1[i][k] * arr2[k][j];
			}
          }
        }
		output(res, n, m);

}

void matrix_addition (float ** arr1, float ** arr2, float ** res, int n, int m )
{
/**
    The function inputs the pointers to the arrays and the sizes of the matrixes,  adds
    matrixes, records the result to the additional array, and outputs the resulting
    matrix on the display.
**/
    int i = 0;
    int j = 0;
            printf("\n RESULT: \n");
            for(i = 0; i < n; i++){
                for(j =0; j < m;j++){
                *(*(res + i) + j) = *(*(arr1 + i) + j) + *(*(arr2 + i) + j);
                }
            }
            output(res, n, m);
}

void subtraction (float ** arr1, float ** arr2, float ** res, int n, int m )
{
    int i = 0;
    int j = 0;
            printf("\n THE RESULT: \n");
            for(i = 0; i < n; i++){
                for(j =0; j < m;j++){
                *(*(res + i) + j) = *(*(arr1 + i) + j) - *(*(arr2 + i) + j);
                }
            }
            output(res, n, m);

}

void number_deduction (float ** array, float ** res, int n1, int m1, int n2, int m2)
{
    int i = 0;
    int j = 0;
    float sub = 0;

                 printf("\n Input subtrahend :  ");
                 scanf("%f", &sub);
                 printf("\n RESULT: \n");
            for(i = 0; i < n1; i++){
                for(j = 0; j < m1; j++){
                    *(*(res + i) + j) =  (*(*(array + i) + j)) - sub;
                }
              }
            output(res, n2, m2);


}

void number_multiplication (float ** array, float ** res, int n, int m)
{
/**
    The procedure of the multiplication of the matrix  by a number,
    outputing on the display.
**/

    int i = 0;
    int j = 0;
    float factor = 0;

                 printf("\n Input factor :  ");
                 scanf("%f", &factor);
                 printf("\n RESULT: \n");

              for(i = 0; i < n; i++){
                for(j = 0; j < m; j++){
                    *(*(res + i) + j) =  (*(*(array + i) + j)) * factor;
                }
              }
            output(res, n, m);
}

void  number_addition (float ** array, float ** res, int n, int m)
{
    int i = 0;
    int j = 0;
    float summand = 0;

                 printf("\n Input summand :  ");
                 scanf("%f", &summand);
                 printf("\n RESULT: \n");
            for(i = 0; i < n; i++){
                for(j = 0; j < m; j++){
                    *(*(res + i) + j) =  (*(*(array + i) + j)) + summand;
                }
              }
            output(res, n, m);
}

void inverse_matrix (float ** array, float **res, int n, int m)
{
/**
    The function inputs the pointer to the matrix and the sizes, determines the inverse
    matrix to the input one, recording the data in an additional array, if it is necessary,
	it puts the changed matrix to the display or returns the pointer to the inverted array
	for the further calculations.
**/

    float DET = 0;
    float ** transit = NULL;
    float ** inverse = NULL;

    if(n == m)
    {
          printf("\n Determinant = [%5.2f ] \n", DET = determinant (array, n) );
          if(DET != 0)
          {
              printf("\n THE ORIGINAL: \n");
              output(array, n, n);

              transit = memory(transit, n, n);
              inverse = memory(inverse, n, n);

                 for(int i = 0; i < n; i++){
                    for(int j = 0; j < n; j++){
                      *(*(transit + i) + j) = minor(array, n, i, j);
                    }
                 }
                printf("\n MINORS MATRIX: \n");
                output(transit, n, n);

                for(int i = 0; i < n; i++){
                    for(int j = 0; j < n; j++){
                      *(*(transit + i) + j) = pow(-1, (i+j)) * *(*(transit + i) + j);
                    }
                 }
                printf("\n ALGEBRIC MATRIX: \n");
                output(transit, n, n);
                transposition(transit, res, n, n) ;
				
                for(int i = 0; i < n; i++){
                    for(int j = 0; j < n; j++){
                   *(*(inverse + i) + j) = 1/DET * *(*(transit + i) + j);
                    *(*(res + i) + j) =  *(*(inverse + i) + j);
                    }
                 }
                printf("\n INVERSED MATRIX: \n");
                output(inverse, n, n);

              for (int i = 0; i < n; i++){
              free (*(inverse + i));
              }
              free(inverse);
              for (int i = 0; i < n; i++){
              free (*(transit + i));
              }
              free(transit);

         }
         else printf("\n The inverse matrix does not exist! \n\n");
    }
    else printf("\n  ERROR! Matrix is not square! \n\n");

}

void transposition (float ** array, float ** res,  int n, int m)
{
/**
    The procedure of the transposition of the matrixes /Flip replacement of the  content
    relative to the main diagonal/ and the following recording to the additional array and outputing
    on the display.
**/
   int i = 0;
   int j = 0;
   float  temp[n][m];

                 for(i = 0; i < n; i++){
                    for(j = 0; j < m; j++){
                       if(i != j){
                        *(*(temp + i) + j) = *(*(array + j) + i);}
                       else temp[i][j] = array[i][j];
                    }
                 }

                 for(i = 0; i < n; i++){
                    for(j = 0; j < m; j++){
                *(*(array +i) + j) = *(*(temp + i) + j);
                 *(*(res + i) + j) =  *(*(array +i) + j);
                    }
                 }
                printf("\n TRANSPOSED MATRIX: \n");
               output(array, n, m);

}

float ** matr(float ** a, int n, int x)
{
    float ** res = 0;
    int i, j, k = 0;

    res = memory(res, n, n);

    for (i = 1; i < n; ++i)
        for (j = 0, k = 0; j < n; ++j, ++k){
            if (j == x){
                --k;
                continue;
            }
            *(*(res + i - 1) + k) = *(*(a + i) + j);
        }
    return res;

	    for (i = 0; i < n; i++){
        free (*(res + i));
        }
        free(res);

}

float determinant (float ** a, int n)
{
/**
    The function inputs the pointer to the array and the size. After the
    calculation it returns the determinant / determinant /.
**/
    if (n == 1)
    return a[0][0];
    if (n == 2)
        return (a[1][1] * a[0][0]) - (a[0][1] * a[1][0]);
    float det = 0, sig = 1;
    for (int i = 0; i < n; ++i){
        det += (sig * a[0][i] * determinant(matr(a, n, i), n - 1));
        sig *= -1;
    }
    return det;
}

float minor (float ** array, int n, int lin, int col)
{
    float min[n][n-1];
    float mat[n-1][n-1];
    float minor = 0;

            for(int i = 0; i < n; i++){
               for(int j = 0; j < n-1; j++){
                 *(*(min + i) + j) = *(*(array + i) + j);
                  if( j >= col) *(*(min + i) + j) = *(*(array + i) + j + 1);
               }
            }
            for(int i = 0; i < n-1; i++){
               for(int j = 0; j < n-1; j++){
                 *(*(mat + i) +j) = *(*(min + i) + j);
                  if(i >= lin) *(*(mat + i) + j) = *(*(min + i + 1) + j);
               }
            }

            float m1 = 1;
            float m2 = 1;

            for(int i = 0 ; i < n-1;i++){
               for(int j = 0; j < n-1; j++){
                 if(i == j)m1 = m1 * *(*(mat + i) + j);
			      if (j == (n - 1 - i - 1))m2 = m2 * *(*(mat + i) + j);
                    }
                }

                minor = m1 - m2;

  return minor;
}

int correct(char number)
{
   int a = 0;
          switch(number)
          {
              case 48:a = 0;return a; break;
              case 49:a = 1;return a; break;
              case 50:a = 2;return a; break;
              case 51:a = 3;return a; break;
              case 52:a = 4;return a; break;
              case 53:a = 5;return a; break;
              case 54:a = 6;return a; break;
              case 55:a = 7;return a; break;
              case 56:a = 8;return a; break;
              case 57:a = 9;return a; break;
              default:printf("\nIncorrect value\n"); a='a';
          }
 return a;
}

int correctSel(char number)
{
   int a = 0;
          switch(number)
          {
              case 49:a = 1;return a; break;
              case 50:a = 2;return a; break;
              default:printf("\nIncorrect value\n"); a='a';
          }
 return a;
}

float correctFill(char number)
{
   float a = 0;
          switch(number)
          {
              case 48:a = 0;return a; break;
              case 49:a = 1;return a; break;
              case 50:a = 2;return a; break;
              case 51:a = 3;return a; break;
              case 52:a = 4;return a; break;
              case 53:a = 5;return a; break;
              case 54:a = 6;return a; break;
              case 55:a = 7;return a; break;
              case 56:a = 8;return a; break;
              case 57:a = 9;return a; break;
              default:printf("\n\nIncorrect value\n\n"); a='a';
          }
 return a;

}
